<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// T
	'theme_bsunited_description' => 'Ubuntu orange and unique font.',
	'theme_bsunited_slogan' => 'Ubuntu orange and unique font',
);
